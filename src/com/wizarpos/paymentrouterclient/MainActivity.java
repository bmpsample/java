package com.wizarpos.paymentrouterclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.wizarpos.aidl.ICloudPay;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements OnClickListener {

	private String param, response, oriTrace;


	private ICloudPay mWizarPayment;
	final ServiceConnection mConnPayment = new PaymentConnection();

	public static Context _Context;

	public static String TEST_IP1 = "113.164.14.80";
	public static int TEST_PORT1 = 11251;
	public static String TEST_IP2 = "113.164.14.80";
	public static int TEST_PORT2 = 11251;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		_Context = getApplicationContext();

		int[] btnIds = { R.id.bind, R.id.unbind,R.id.setParams,
			R.id.login, R.id.settle, R.id.printlast,
			R.id.payCash, R.id.voidSale
		};
		for (int id : btnIds) {
			findViewById(id).setOnClickListener(this);
		}

//		Switch sw = (Switch) findViewById(R.id.switch_test);
//		sw.setChecked(IsTestEnvironment);
//		sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				IsTestEnvironment = isChecked;
//			}
//		});
	}

	@Override
	public void onBackPressed() {
		System.exit(0);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindPaymentRouter();
	}

	class PaymentConnection implements ServiceConnection {
		@Override
		public void onServiceConnected(ComponentName compName, IBinder binder) {
			Log.d("onServiceConnected", "compName: " + compName);
			mWizarPayment = ICloudPay.Stub.asInterface(binder);
			showResponse("Connect Success!");
		}

		@Override
		public void onServiceDisconnected(ComponentName compName) {
			Log.d("onServiceDisconnected", "compName: " + compName);
			mWizarPayment = null;
			showResponse("Disconnect Success!");
		}
	};

	private void bindPaymentRouter() {
		if (mWizarPayment == null) {
			Intent intent = new Intent("com.wizarpos.aidl.ICloudPay");
			intent.setPackage("com.wizarpos.napas");
			bindService(intent, mConnPayment, BIND_AUTO_CREATE);
		}
	}
	private void unbindPaymentRouter() {
		if (mWizarPayment != null) {
			unbindService(mConnPayment);
			mWizarPayment = null;
		}
	}

	public void showResponse(String response) {
		this.response = response;
		showResponse();

	}

	public void showResponse() {
		setTextById(R.id.param, param);
		setTextById(R.id.result, response);
	}
	private void setTextById(int id, CharSequence text) {
		((TextView)findViewById(id)).setText(text);
	}

	@Override
	public void onClick(final View view) {
		final int btnId = view.getId();
		setTextById(R.id.method, ((TextView)view).getText());

		param = "";
		response = "";
		switch(btnId) {
		case R.id.bind:				bindPaymentRouter();    break;
		case R.id.unbind:			unbindPaymentRouter();  break;
//		case R.id.voidSale:			showInputDialog("Please input Transaction Num",6,btnId);		break;
		default:
			if (mWizarPayment == null) {
				response = "Please click [ConnectPaymentRouter First]!";
			} else if(!mWizarPayment.asBinder().isBinderAlive()){
				response = "Please click [ConnectPaymentRouter First]!";
			}else if (null == (param = getParam(btnId))) {
				response = "Call parameter failed!";
			}
			if (response == "") {
				createAsyncTask().execute(btnId);
				return;
			}
			break;
		}
		showResponse();
	}

	private String getParam(int btnId) {
		JSONObject jsonObject = new JSONObject();
		try {
			switch(btnId) {
			case R.id.payCash:				setParam4PayCash(jsonObject);	break;
			case R.id.voidSale:				setParam4VoidSale(jsonObject);	break;
			case R.id.printlast:			setParam4getPrintLast		(jsonObject);	break;
			case R.id.settle:				setParam4settle				(jsonObject);	break;
			case R.id.setParams:			setParam4SetParams(jsonObject);	break;
			case R.id.login:				break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return jsonObject.toString();
	}

	private AsyncTask<Integer, Void, String> createAsyncTask() {
		return new AsyncTask<Integer, Void, String>() {
			protected void onPreExecute() {
				showResponse("...");
			}
			protected String doInBackground(Integer...btnIds) {
				Log.d("doInBackground", "Request: " + param + " mWizarPayment: " + mWizarPayment);

				String result = "Skipped";
				try {
					switch(btnIds[0]) {
					case R.id.payCash:
					case R.id.voidSale:
						result = mWizarPayment.transact			(param);							break;
					case R.id.printlast:		result = mWizarPayment.printLast		(param);	break;
					case R.id.settle:			result = mWizarPayment.settle			(param);	break;
					case R.id.setParams:		result = mWizarPayment.setParams		(param);	break;
					case R.id.login:			result = mWizarPayment.login();						break;
					}
				} catch (RemoteException e) {
					result = e.getMessage();
				}

				Log.d("doInBackground", "Response: " + result);

				return result;
			}
			protected void onPostExecute(String result) {
				showResponse(result);
			}
		};
	}


	private void setParam4PayCash(JSONObject jsonObject) throws JSONException {
		jsonObject.put("TransType", 1);
		jsonObject.put("TransAmount", "000000000111");
		jsonObject.put("timeOut", 30);// 10 means timeout after 10 seconds
	}

	private void setParam4VoidSale(JSONObject jsonObject) throws JSONException {
		jsonObject.put("passWord", "123456");
		jsonObject.put("TransType", 101);
	}

	private void setParam4Refund(JSONObject jsonObject) throws JSONException {
		jsonObject.put("passWord", "123456");
		jsonObject.put("TransType", 100);
	}

	private void setParam4getPrintLast(JSONObject jsonObject) throws JSONException {
	}

	private void setParam4settle(JSONObject jsonObject) throws JSONException {
		jsonObject.put("TransType", 21);
	}

	private void setParam4SetParams(JSONObject jsonObject) throws JSONException {
		jsonObject.put("PrimaryIP",TEST_IP1);
		jsonObject.put("SecondaryIP",TEST_IP2);
		jsonObject.put("PrimaryPort",TEST_PORT1);
		jsonObject.put("SecondaryPort",TEST_PORT2);
		jsonObject.put("passWord", "123456");

	}

	private void showInputDialog(String title,int maxInput,final int btnID) {
		oriTrace = "";
		final EditText editText = new EditText(MainActivity.this);
		editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		InputFilter[] filters = {new InputFilter.LengthFilter(maxInput)};
		editText.setFilters(filters);
		AlertDialog.Builder inputDialog =
			new AlertDialog.Builder(MainActivity.this);
		inputDialog.setTitle(title).setView(editText);
		inputDialog.setNegativeButton("Cancel",null);
		inputDialog.setPositiveButton("Confirm",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (btnID){
					case R.id.voidSale:			oriTrace = editText.getText().toString(); 		break;
					}

					if (mWizarPayment == null) {
						response = "Please click [ConnectPaymentRouter First]!";
					} else if (null == (param = getParam(btnID))) {
						response = "Call parameter failed!";
					}
					if (response == "") {
						createAsyncTask().execute(btnID);
						return;
					}

					showResponse();
				}
			}).show();
	}




}
