/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.wizarpos.aidl;
public interface ICloudPay extends android.os.IInterface
{
  /** Default implementation for ICloudPay. */
  public static class Default implements com.wizarpos.aidl.ICloudPay
  {
    @Override public java.lang.String getPOSInfo(java.lang.String jsonData) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String transact(java.lang.String jsonData) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String settle(java.lang.String jsonData) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String printLast(java.lang.String jsonData) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String login() throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String setParams(java.lang.String jsonData) throws android.os.RemoteException
    {
      return null;
    }
    //support change IP/PORT/TPDU/Nii....

    @Override public void cancelTransaction() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.wizarpos.aidl.ICloudPay
  {
    private static final java.lang.String DESCRIPTOR = "com.wizarpos.aidl.ICloudPay";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.wizarpos.aidl.ICloudPay interface,
     * generating a proxy if needed.
     */
    public static com.wizarpos.aidl.ICloudPay asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.wizarpos.aidl.ICloudPay))) {
        return ((com.wizarpos.aidl.ICloudPay)iin);
      }
      return new com.wizarpos.aidl.ICloudPay.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_getPOSInfo:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.getPOSInfo(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_transact:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.transact(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_settle:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.settle(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_printLast:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.printLast(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_login:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.login();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_setParams:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.setParams(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_cancelTransaction:
        {
          data.enforceInterface(descriptor);
          this.cancelTransaction();
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.wizarpos.aidl.ICloudPay
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public java.lang.String getPOSInfo(java.lang.String jsonData) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(jsonData);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPOSInfo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPOSInfo(jsonData);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String transact(java.lang.String jsonData) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(jsonData);
          boolean _status = mRemote.transact(Stub.TRANSACTION_transact, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().transact(jsonData);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String settle(java.lang.String jsonData) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(jsonData);
          boolean _status = mRemote.transact(Stub.TRANSACTION_settle, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().settle(jsonData);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String printLast(java.lang.String jsonData) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(jsonData);
          boolean _status = mRemote.transact(Stub.TRANSACTION_printLast, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().printLast(jsonData);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String login() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_login, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().login();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String setParams(java.lang.String jsonData) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(jsonData);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setParams, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setParams(jsonData);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      //support change IP/PORT/TPDU/Nii....

      @Override public void cancelTransaction() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancelTransaction, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().cancelTransaction();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static com.wizarpos.aidl.ICloudPay sDefaultImpl;
    }
    static final int TRANSACTION_getPOSInfo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_transact = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_settle = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_printLast = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_login = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_setParams = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_cancelTransaction = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    public static boolean setDefaultImpl(com.wizarpos.aidl.ICloudPay impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.wizarpos.aidl.ICloudPay getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public java.lang.String getPOSInfo(java.lang.String jsonData) throws android.os.RemoteException;
  public java.lang.String transact(java.lang.String jsonData) throws android.os.RemoteException;
  public java.lang.String settle(java.lang.String jsonData) throws android.os.RemoteException;
  public java.lang.String printLast(java.lang.String jsonData) throws android.os.RemoteException;
  public java.lang.String login() throws android.os.RemoteException;
  public java.lang.String setParams(java.lang.String jsonData) throws android.os.RemoteException;
  //support change IP/PORT/TPDU/Nii....

  public void cancelTransaction() throws android.os.RemoteException;
}
